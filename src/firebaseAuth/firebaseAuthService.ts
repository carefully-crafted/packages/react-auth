import { FirebaseApp, FirebaseError } from "firebase/app";
import {
  Auth,
  createUserWithEmailAndPassword,
  FacebookAuthProvider,
  GoogleAuthProvider,
  OAuthProvider,
  signInWithCustomToken,
  signInWithEmailAndPassword,
  signInWithPopup,
  signOut,
  User as FirebaseAuthUser,
  UserCredential,
  fetchSignInMethodsForEmail,
  getAuth,
} from "firebase/auth";

export type LoginResult = UserCredential | "userCancelled";

export class FirebaseAuthService {
  private auth: Auth;
  private readonly _listeners: Array<
    (currentUser: FirebaseAuthUser | null) => void
  > = [];
  private _currentUser: FirebaseAuthUser | null | undefined;

  constructor(app?: FirebaseApp) {
    this._currentUser = undefined;
    this.auth = getAuth(app);
    this.auth.onAuthStateChanged((user) => {
      this._currentUser = user;
      for (const listener of this._listeners) {
        listener(user);
      }
    });
  }

  get currentUser() {
    return this._currentUser;
  }

  onAuthStateChanged(
    listener: (currentUser: FirebaseAuthUser | null) => void,
  ): () => void {
    this._listeners.push(listener);
    return () => {
      const i = this._listeners.indexOf(listener);
      if (i >= 0) {
        this._listeners.splice(i, 1);
      }
    };
  }

  signInWithApple(): Promise<LoginResult> {
    const provider = new OAuthProvider("apple.com");
    provider.addScope("name");
    provider.addScope("email");
    return this.signIn(provider);
  }

  signInWithFacebook(): Promise<LoginResult> {
    return this.signIn(new FacebookAuthProvider());
  }

  signInWithGoogle(): Promise<LoginResult> {
    return this.signIn(new GoogleAuthProvider());
  }

  async signInWithEmailAndPassword(
    email: string,
    password: string,
  ): Promise<UserCredential> {
    const credential = await signInWithEmailAndPassword(
      this.auth,
      email,
      password,
    );
    return credential;
  }

  async createUserWithEmailAndPassword(
    email: string,
    password: string,
  ): Promise<FirebaseAuthUser> {
    let credential = await createUserWithEmailAndPassword(
      this.auth,
      email,
      password,
    );
    credential = await signInWithEmailAndPassword(this.auth, email, password);
    return credential.user;
  }

  async checkUserExists(email: string): Promise<boolean> {
    const methods = await fetchSignInMethodsForEmail(this.auth, email);
    return methods.length > 0;
  }

  private async signIn(
    provider: OAuthProvider | FacebookAuthProvider | GoogleAuthProvider,
  ): Promise<LoginResult> {
    try {
      const user = await signInWithPopup(this.auth, provider);
      return user;
    } catch (e) {
      if (
        e instanceof FirebaseError &&
        (e.code === "auth/popup-closed-by-user" ||
          e.code === "auth/user-cancelled")
      ) {
        return "userCancelled";
      }
      throw e;
    }
  }

  signInWithCustomToken(customToken: string): Promise<any> {
    return signInWithCustomToken(this.auth, customToken);
  }

  logout(): Promise<void> {
    return signOut(this.auth);
  }

  getIdToken(): Promise<string> | undefined {
    return this.auth.currentUser?.getIdToken();
  }
}
