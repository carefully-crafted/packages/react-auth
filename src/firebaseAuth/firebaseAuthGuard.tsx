import React, { createContext, useContext, useEffect, useState } from "react"
import { FirebaseAuthService } from "./firebaseAuthService"
import { User as FirebaseAuthUser } from "firebase/auth"

export const AuthUserContext = createContext<FirebaseAuthUser | null>(null);

interface FirebaseAuthGuardProps {
  readonly firebaseAuthService: FirebaseAuthService;
  readonly onSignedOut: () => void;
  readonly loadingComponent?: React.ReactNode;
  readonly signedOutComponent?: React.ReactNode;
  readonly children?: React.ReactNode
}

export const FirebaseAuthGuard = (props: FirebaseAuthGuardProps) => {
  const [authUser, setAuthUser] = useState(props.firebaseAuthService.currentUser)

  useEffect(() => {
    const removeListener = props.firebaseAuthService.onAuthStateChanged((authUser) => {
      setAuthUser(authUser)
    })
    return removeListener;
  }, [])

  useEffect(() => {
    if (authUser === null) {
      props.onSignedOut()
    }
  }, [authUser])

  if (authUser === undefined) {
    return <>{props.loadingComponent}</>
  } else if (authUser === null) {
    return <>{props.signedOutComponent}</>
  }

  return (
    <AuthUserContext.Provider value={authUser}>
      {props.children}
    </AuthUserContext.Provider>
  )
}

export const useFirebaseAuthUser = () => {
  const authUser = useContext(AuthUserContext);
  if (!authUser) {
    throw new Error("useFirebaseAuthUser was used outside of FirebaseAuthGuard!")
  }

  return authUser;
}
